package exception;

public class NumberPokemonException extends Exception {
    public NumberPokemonException(String message) {
        super(message);
    }
}
