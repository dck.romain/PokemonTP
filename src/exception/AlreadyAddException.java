package exception;

public class AlreadyAddException extends Exception {
    public AlreadyAddException(String message) {
        super(message);
    }
}
