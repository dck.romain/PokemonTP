package pokemon;

public class Dracaufeu extends Pokemon {

    public Dracaufeu(String name, int life, int power) {
        super(name, life, power, "Dracaufeu!", "DRACOOOO", "Fiiiuuuu");
    }
}
