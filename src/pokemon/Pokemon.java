package pokemon;

public class Pokemon {
    private String name;
    private int life;
    private int maxLife;
    private int power;
    private String shout;
    private String shoutDefeat;
    private String shoutVictory;

    public Pokemon(String name, int life, int power, String shout, String shoutVictory, String shoutDefeat) {
        this.name = name;
        this.life = life;
        this.maxLife = life;
        this.power = power;
        this.shout = shout;
        this.shoutDefeat = shoutDefeat;
        this.shoutVictory = shoutVictory;
    }

    public String getShoutVictory() {
        return shoutVictory;
    }

    public int getLife() {
        return life;
    }

    public int getPower() {
        return power;
    }

    public String getShoutDefeat() {
        return shoutDefeat;
    }

    public String getShout() {
        return shout;
    }

    public boolean attack(Pokemon pok) {

        return pok.receiveAttack(power);
    }

    public boolean receiveAttack(int damage) {
        if (damage >= life) {
            life = 0;
            return true;
        } else {
            life -= damage;
            return false;
        }
    }

    public void heal()
    {
        life = maxLife;
    }

    public String getName() {
        return name;
    }
}