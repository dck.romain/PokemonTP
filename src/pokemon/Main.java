package pokemon;

import exception.AlreadyAddException;
import exception.NumberPokemonException;

public class Main {
    public static void main(String[] args) throws NumberPokemonException, AlreadyAddException {
        Tournament tournament = new Tournament(5);

        Pokemon pikachu = new Pikachu("Picka", 200, 50);

        try {
            tournament.add(pikachu);

            tournament.add(new Dracaufeu("Draco", 200, 150));
            tournament.add(new Evoli("Evoli", 100, 20));
            tournament.add(new Dracaufeu("DracoPlusPuissant", 300, 250));
            tournament.add(pikachu);
        } catch (AlreadyAddException | NumberPokemonException e) {
            System.out.println(e.getMessage());
        }
        tournament.startTournament();
    }
}