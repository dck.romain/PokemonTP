package pokemon;

public class Evoli extends Pokemon {

    public Evoli(String name, int life, int power) {
        super(name, life, power, "Evoli!", "EVOLIIIII!!!!", "Voliiiii...");
    }
}
