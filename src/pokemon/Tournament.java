package pokemon;

import exception.AlreadyAddException;
import exception.NumberPokemonException;

import java.util.ArrayList;

public class Tournament {
    private ArrayList<Pokemon> pokeList;
    private int maxPokemon;

    public Tournament(int maxPokemon) {
        this.maxPokemon = maxPokemon;
        this.pokeList = new ArrayList<>();
    }

    public void add(Pokemon pokemon) throws NumberPokemonException, AlreadyAddException {

        if (pokeList.contains(pokemon)) {
            throw new AlreadyAddException("Ce pokémon a déjà été inscrit au tournoi, impossible de l'inscrire");
        } else if (maxPokemon > pokeList.size()) {
            pokeList.add(pokemon);
        } else {
            throw new NumberPokemonException("Le nombre de pokémon autorisé est dépassé, impossible de 'inscrire.");
        }
    }

    public void startTournament() {
        while (pokeList.size() != 1) {
            Pokemon first = pokeList.get(0);
            Pokemon second = pokeList.get(1);
            Pokemon loser = fight(first, second);
            pokeList.remove(loser);
        }
        System.out.println(pokeList.get(0).getName()+" a remporté le Tournoi");
    }

    public Pokemon fight(Pokemon a, Pokemon b) {
        boolean isKO;

        System.out.println(a.getShout() + "\n" + b.getShout());

        while (a.getLife() != 0 && b.getLife() != 0) {
             isKO = a.attack(b);
            System.out.println(a.getName()+" attaque " + b.getName() + ", il subit " + a.getPower() + " dégat");
            if (isKO) {
                System.out.println(b.getName() + " est KO.");
                System.out.println(b.getShoutDefeat() + "\n" + a.getShoutVictory());
                a.heal();
                return b;
            }

            isKO = b.attack(a);
            System.out.println(b.getName()+" attaque " + a.getName() + ", il subit " + b.getPower() + " dégat");
            if (isKO) {
                System.out.println(a.getName() + " est KO.");
                System.out.println(a.getShoutDefeat() + "\n" + b.getShoutVictory());
                b.heal();
                return a;
            }
        }
        return null;
    }
}