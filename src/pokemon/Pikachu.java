package pokemon;

public class Pikachu extends Pokemon {

    public Pikachu(String name, int life, int power) {
        super(name, life, power, "Pikachu!", "PIKAAAAAA", "Chuuuuuu");
    }
}
